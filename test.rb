require 'minitest/autorun'
require 'rest_client'
require 'json'


class DriverAPITestLoad < MiniTest::Unit::TestCase
  def setup
    @load1 = RestClient.post(
        "http://localhost:9292/import", 
        '[{"id":3,"name":"Johnny B. Goode","license_number":"12-234-45"}]'
    )
    # test existing rows
    @load2 = RestClient.post(
        "http://localhost:9292/import", 
        '[
            {"id":2,"name":"Eyal Golan","license_number":"11-288-10"},
            {"id":3,"name":"Johnny B. Goode","license_number":"12-234-45"}, 
            {"id":3,"name":"Johnny B. Goode","license_number":"12-234-45"}, 
            {"id":7,"name":"Janice Joplin","license_number":"65-112-10"}
         ]'
    )
    r_get = RestClient.get("http://localhost:9292/drivers/3")
    @data = JSON.parse r_get.body
    @errors = JSON.parse @load2.body
  end

  def test_id
    assert_equal 3, @data['id']
  end

  def test_errors
    assert_equal 2, @errors['errors'].length
  end
end

