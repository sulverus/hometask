Gett home task (drivers)

### Usage
* `bundle`
* `bundle exec rake db:migrate`
* `bundle exec rakeup`

### API
* import - load drivers using post request body
* drivers/:id - get driver by id