require 'sinatra'
require 'sinatra/activerecord'
require 'json'
require './models'
require './settings'

class DriverAPI < Sinatra::Application
    before do
        content_type 'application/json'
    end

    get '/' do
        {
            api: "Drivers app", version: 1,
            read: '/drivers/:id',
            import: '/import'
        }.to_json
    end

    get '/drivers/:id' do
        @driver = Driver.find_by_id(params[:id])
        if !@driver
            halt 404, "Driver not found"
        end
        return @driver.to_json
    end

    post '/import' do
        # check body
        begin
            request.body.rewind
            @drivers = JSON.parse request.body.read
            if !@drivers
                return status 400
            end
        rescue
            return status 400
        end
        
        # try to add all drivers
        errors = Array.new()
        @drivers.each do |driver|
            begin
                id = driver['id'].to_i
                @d = Driver.new(
                    name: driver['name'], id: id,
                    license_number: driver['license_number']
                )
                if !@d.save
                    errors.push(id)
                end
            rescue
                errors.push(id)
            end
        end

        if errors.length > 0
            return {errors:errors}.to_json
        end
        return status 201
    end
end
