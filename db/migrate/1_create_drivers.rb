class CreateDrivers < ActiveRecord::Migration
  def up
    create_table :drivers do |t|
      t.string :name
      t.string :license_number
    end
  end

  def down
    drop_table :users
  end
end
